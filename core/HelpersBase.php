<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 13.06.2016
 * Time: 17:38
 */
class HelpersBase
{

    public static function truncateWords($string, $count, $suffix = '...')
    {
        $words = preg_split('/(\s+)/u', trim($string), null, PREG_SPLIT_DELIM_CAPTURE);
        if (count($words) / 2 > $count) {
            return implode('', array_slice($words, 0, ($count * 2) - 1)) . $suffix;
        } else {
            return $string;
        }
    }


    public static function avatar( $mail = null, $s = 65, $d = 'mm', $r = 'g', $img = false, $atts = [] ) {

        if ( !is_null($mail) ) {
            $url = 'https://www.gravatar.com/avatar/';
            $url .= md5( strtolower( trim( $mail ) ) );
            $url .= "?s=$s&d=$d&r=$r";
            if ( $img ) {
                $url = '<img src="' . $url . '"';
                foreach ( $atts as $key => $val )
                    $url .= ' ' . $key . '="' . $val . '"';
                $url .= ' />';
            }
        } else {
            $url = 'http://keenthemes.com/assets/bootsnipp/img1-small.jpg';
        }
        return $url;
    }
    
    
}