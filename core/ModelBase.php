<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 13.06.2016
 * Time: 14:41
 */
class ModelBase
{
    protected $dbase;

    /**
     * @return mixed
     */
    public function getDbase()
    {
        return $this->dbase;
    }

    public function __construct()
    {
        $this->dbase = AppBase::getDb();
    }
    
}