<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 11.06.2016
 * Time: 18:55
 */

require_once ( SERVER_ROOT . DS . 'config' . DS . 'main.php' );

function __autoload( $class_name )
{
    $base_path = SERVER_ROOT . DS . 'core' . DS . str_replace('Base','',ucfirst($class_name)).'Base.php';
    $controllers_path = SERVER_ROOT . DS . 'controllers' . DS . str_replace('Controller','',ucfirst($class_name)) . 'Controller.php';
    $model_path = SERVER_ROOT . DS . 'models' . DS . ucfirst($class_name).'.php';


        if ( file_exists($base_path) ) {
            require_once ($base_path);
        }
        elseif ( file_exists($controllers_path) ) {
            require_once ($controllers_path);
        }
        elseif ( file_exists($model_path) ) {
            require_once ($model_path);
        }

}