<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 13.06.2016
 * Time: 22:41
 */
class SessionBase
{
    protected static $flash;
    protected static $class;

    /**
     * @param mixed $class
     */
    public static function setClass($class)
    {
        self::$class = $class;
    }

    /**
     * @return mixed
     */
    public static function getClass()
    {
        return self::$class;
    }

    /**
     * @return mixed
     */
    public static function getFlash()
    {
        return self::$flash;
    }


    public static function setFlash( $message = null, $class = 'info')
    {
        self::$flash = $message;
        self::setClass($class);
    }

    /**
     * @return mixed
     */
    public static function hasFlash()
    {
        return !is_null(self::$flash);
    }

    public static function showFlash()
    {
        echo self::$flash;
        self::$flash = null;
        self::$class = null;
    }

}