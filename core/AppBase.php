<?php

/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 3:18
 */
class AppBase
{
    protected static $router;
    protected static $db;

    /**
     * @return mixed
     */
    public static function getDb()
    {
        return self::$db;
    }

    /**
     * @return mixed
     */
    public static function getRouter()
    {
        return self::$router;
    }

    public static function run($uri)
    {
        self::$router = new RouterBase($uri);
        self::$db = new DbBase( ConfigBase::get('db_dsn') , ConfigBase::get('db_username') , ConfigBase::get('db_passwd') );
        $controller_class = self::$router->getController();
        $controller_action = self::$router->getAction();


        // Позволил себе предположить что здесь я смогу перехватывать любые ексепшны, ибо здесь весь корень всего зла
        // Как проходная на заводе с КПП. Где создаются все обьекты, и при любом возникшем внутри обьектa ексепшене
        // Я смогу перехватить его именно здесь. Дальше в кетче можно развить логику с редиректами на спец.страницу с оформлением ошибки.
        try {
            if ( class_exists($controller_class) ) {
                $controller_obj = new $controller_class();
                if ( method_exists($controller_obj,$controller_action ) ) {
                    $controller_obj->$controller_action();
                } else {
                    throw new Exception('В контролллере '.$controller_class.' нет метода '.$controller_action.'.');
                }
            } else {
                throw new Exception('Нет такого контроллера '.$controller_class);
            }
        } catch (Exception $e) {
            var_dump( $e->getMessage() .' '. __CLASS__ . 'в файле ' . __FILE__);
            var_dump( 'В этом месте можно было бы еще вписать логику перехвата ошибок и рендера спецстранички для ероров. Но вылизывать это можно бесконечно.' );
        }

}

}