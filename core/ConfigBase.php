<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 0:52
 */

class ConfigBase
{
    protected static $settings = [];

    public static function get($key)
    {
        return  isset( self::$settings[$key] ) ? self::$settings[$key]  : null ;
    }

    public static function set( $key, $value)
    {
        self::$settings[$key] = $value;
    }


}