<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 12.06.2016
 * Time: 18:06
 */
?>

<div class="row">
    <div class="col-md-4">
        <h1 class="titles">Use. Test. Enjoy ?&gt;</h1>
    </div>
    <div class="col-md-8">
        <p style="    padding-top: 16px;     text-align: right;">
            <a class="btn btn-success" href="/blog/add">Создать запись</a></p>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">

        <div class="page-header">
            <h3>Популярное Очевидное Невероятное</h3>
            <p>Это должен обсудить каждый...</p>
        </div>

        <div class="carousel slide" id="myCarousel">
            <div id="w0" class="carousel-inner" role="listbox">

                <? foreach ( $popular as $num => $popular_entry ) { ?>
                
                <div class="item" data-key="<?= $popular_entry['entries_id'] ?>">
                    <ul class="thumbnails">
                        <li class="col-sm-12">
                            <div class="fff">
                                <div class="thumbnail">
                                    <a href="#"><img src="http://placehold.it/1110x240" alt=""></a>
                                </div>
                                <div class="caption">
                                    <h4><?= $popular_entry['title'] ?></h4>
                                    <p><?= HelpersBase::truncateWords($popular_entry['text'], 20)  ?></p>
                                    <a class="btn btn-danger" href="/blog/entry/<?= $popular_entry['entries_id'] ?>">Комментариев: <b><?= $popular_entry['count_comments'] ?></b> </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                    
                <? } ?>

                
            </div>                <nav>
                <ul class="control-box pager">
                    <li><a data-slide="prev" href="#myCarousel" class="">< СЮДА</a></li>
                    <li><a data-slide="next" href="#myCarousel" class="">ТУДА ></a></li>
                </ul>
            </nav>
            <!-- /.control-box -->
        </div><!-- /#myCarousel -->
    </div><!-- /.col-xs-12 -->
</div>

<div class="row">
<div class="page-header col-sm-12">
    <h3>Последние поступления</h3>
    <p>Можно заглянуть, бывает интересно</p>
</div>
<!--    Было бы приятно засунуть все эти форы в отдельные рендеры. Но для этого необходимо было бы снова дорабатывать класс Вью.-->
<? foreach ( $entries as $num => $entry) { ?>
    <div class="col-sm-12">
        <div id="tb-testimonial" class="testimonial testimonial-primary">
            <div class="testimonial-section">
                <?=  HelpersBase::truncateWords($entry['entry_text'], 25)   ?>
            </div>
            <div class="testimonial-desc">
                <img src="<?= HelpersBase::avatar($entry['authors_mail'])?>" alt="" />
                <div class="testimonial-writer">
                    <div class="testimonial-writer-name"><a href="blog/entry/<?= $entry['entries_id']  ?>"><?= $entry['entry_title'] ?></a></div>
                    <div class="testimonial-writer-name-small">Написал: <b><?= $entry['author_name']  ?></b>
                        <?= date('d.m.Y' , $entry['entry_created_at'])   ?>
                        в <?= date('H:i:s' , $entry['entry_created_at'])  ?>
                    </div>
                    <div class="testimonial-writer-name-small">Комментариев: <b><?= count($model->getCommentsById($entry['entries_id']))  ?></b></div>
                </div>
            </div>
        </div>
    </div>
<? } ?>
</div>