<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css" >
    <link rel="stylesheet" href="/css/slider.css" >

    <meta charset="UTF-8">
    <title>БОГГГЕР</title>
</head>
<body>
<nav class="navbar navbar-default" style="    margin-bottom: 0;">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Блог чтоле?</a>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div class="wrap">
    <div class="container">
        
        <?  if ( SessionBase::hasFlash() ) { ?>
            <div class="alert alert-<?= SessionBase::getClass() ?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> <?= SessionBase::showFlash() ?>
            </div>
        <?  } ?>
        
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; For V-Jet Clean Test powered <?= date('Y') ?></p>
    </div>
</footer>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/slider.js"></script>
</body>
</html>